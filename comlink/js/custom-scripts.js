(function($) {
    
  'use strict';


  /**
   * =====================================
   * Function for windows height and width      
   * =====================================
   */
  function windowSize( el ) {
    var result = 0;
    if("height" == el)
        result = window.innerHeight ? window.innerHeight : $(window).height();
    if("width" == el)
      result = window.innerWidth ? window.innerWidth : $(window).width();

    return result; 
  }


  /**
   * =====================================
   * Function for email address validation         
   * =====================================
   */
  function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
  };


  /**
   * =====================================
   * Function for windows height and width      
   * =====================================
   */
  function deviceControll() {
    if( windowSize( 'width' ) < 768 ) {
      $('body').removeClass('desktop').removeClass('tablet').addClass('mobile');
    }
    else if( windowSize( 'width' ) < 992 ){
      $('body').removeClass('mobile').removeClass('desktop').addClass('tablet');
    }
    else {
      $('body').removeClass('mobile').removeClass('tablet').addClass('desktop');
    }
  }




  $(window).on('resize', function() {

    deviceControll();

  });



  $(document).on('ready', function() {

    deviceControll();



    /**
     * =======================================
     * Top Navigaion Init
     * =======================================
     */
    // var navigation = $('#js-navbar-menu').okayNav({
    //   toggle_icon_class: "okayNav__menu-toggle",
    //   toggle_icon_content: "<span /><span /><span /><span /><span />"
    // });



    /**
     * =======================================
     * Top Fixed Navbar
     * =======================================
     */
    $(document).on('scroll', function() {
      var activeClass = 'fixed',
          ActiveID        = '#navigation',
          scrollPos       = $(this).scrollTop();

      if( scrollPos > 700 ) {
        $( ActiveID ).addClass( activeClass );
      } else {
        $( ActiveID ).removeClass( activeClass );
      }
    });




    /**
     * =======================================
     * NAVIGATION SCROLL
     * =======================================
     */
    var TopOffsetId = '.navbar-brand';
    $('#navbar-nav').onePageNav({
        currentClass: 'active',
        scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
        scrollSpeed: 1000,
        scrollOffset: 50
     //   scrollOffset: Math.abs( $( TopOffsetId ).outerHeight() - 1 )
    });

    $('.btn, a.btn-nav').on('click', function (e) {
      e.preventDefault();

      var target = this.hash,
          scrollOffset = 50,
          $target = ( $(target).offset() || { "top": NaN }).top;

      $('html, body').stop().animate({
        'scrollTop': $target - scrollOffset
      }, 900, 'swing', function () {
        window.location.hash = target;
      });

    });


    /**
     * =======================================
     * PopUp Item Script
     * =======================================
     */
    $('.popup-video').magnificPopup({
      //disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: true,
      fixedContentPos: true
    });


    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    function onYouTubeIframeAPIReady() {
      player = new YT.Player('myplayer', {
        events: {
          'onStateChange': onPlayerStateChange
        }
      });
    }

    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        $(".div_popup_overlay").fadeIn();
      }
      else if (event.data == YT.PlayerState.PAUSED || event.data == YT.PlayerState.ENDED) {
        $(".div_popup_overlay").fadeOut();
      }
    }
  


     /**
     * =======================================
     * TESTIMONIAL SYNC WITH CLIENTS
     * =======================================
     */
    var testimonialSlider = $(".testimonial-wrapper"); // client's message
    testimonialSlider.owlCarousel({
      singleItem :        true,
      autoPlay :          3000,
      slideSpeed :        500,
      paginationSpeed :   500,
      autoHeight :        false,
      navigation:         false,
      pagination:         true,
      // transitionStyle:    "fade"
    });



    // Navbar mobile

    $(function(){
    $('#navbar-nav').slicknav({
        label: ''

    });
   });  

    



    /**
     * ============================
     * CONTACT FORM 2
     * ============================
    */
    $("#contact-form").on('submit', function(e) {
      e.preventDefault();
      var success = $(this).find('.email-success'),
        failed = $(this).find('.email-failed'),
        loader = $(this).find('.email-loading'),
        postUrl = $(this).attr('action');

      var data = {
        name: $(this).find('.contact-name').val(),
        email: $(this).find('.contact-email').val(),
        subject: $(this).find('.contact-subject').val(),
        message: $(this).find('.contact-message').val()
      };

      if ( isValidEmail(data['email']) && (data['message'].length > 1) && (data['name'].length > 1) ) {
        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          beforeSend: function() {
            loader.fadeIn(1000);
          },
          success: function(data) {
            loader.fadeOut(1000);
            success.delay(500).fadeIn(1000);
            failed.fadeOut(500);
          },
          error: function(xhr) { // if error occured
            loader.fadeOut(1000);
            failed.delay(500).fadeIn(1000);
            success.fadeOut(500);
          },
          complete: function() {
            loader.fadeOut(1000);
          }
        });
      } else {
        loader.fadeOut(1000);
        failed.delay(500).fadeIn(1000);
        success.fadeOut(500);
      }

      return false;
    });


      var config = {
        afterReveal: chartsanimate
      }


      // Scroll Reveal
      window.sr = ScrollReveal(config);
      sr.reveal('.row');


  });

                            var color = Chart.helpers.color;
                            var barChartData = {
                                labels: ["11/12", "12/13", "13/14", "14/15", "15/16", "16/17",],
                                datasets: [{
                                    label: 'City',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        5, 20, 40, 1800, 1900, 2500
                                    ]
                                }, {
                                    label: 'Regional',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                         2100, 2200, 2500, 4200, 4800, 5200
                                    ]
                                },
                                {
                                    label: 'Rural',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                         200, 200, 200, 500, 600, 1200
                                    ]
                                }]

                            };


                             var clientsdata = {
                                labels: ["11/12", "12/13", "13/14", "14/15", "15/16", "16/17",],
                                datasets: [{
                                    label: 'Number of Clients',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        18, 45, 55, 145, 160, 260
                                    ]
                                }]

                            };


                             var renenue_chart = {
                                labels: ["", "2011-12FY", "2012-13FY", "2013-14FY", "2014-15FY", "2015-16FY", "2016-17FY",],
                                datasets: [{
                                    label: 'Revenue',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                       0, 2.0, 2.2, 4.3, 9.0, 9.7, 10.8
                                    ]
                                }
                                ]

                            };

                                var randomScalingFactor = function() {
                                    return Math.round(Math.random() * 100);
                                };

                                var pie1 = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: [
                                                91, 9
                                            ],
                                            backgroundColor: [
                                                window.chartColors.blue,
                                                window.chartColors.orange,
                                            ],
                                            label: 'Financial Performance'
                                        }],
                                        labels: [
                                            "Expense",
                                            "Profit",
              
                                        ]
                                    },
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        tooltips: {
                                            mode: 'nearest'
                                        },
                                        title: {
                                            display: true,
                                            text: 'Financial Perfomance % 2016 - 17FY',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        },
                                        animation: {
                                            animateScale: true,
                                            animateRotate: true
                                        }
                                    }
                                };

                                var donut = {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                            ],
                                            backgroundColor: [
                                                window.chartColors.red,
                                                window.chartColors.orange,
                                                window.chartColors.yellow,
                                                window.chartColors.green,
                                                window.chartColors.blue,
                                            ],
                                            label: 'Dataset 1'
                                        }],
                                        labels: [
                                            "Red",
                                            "Orange",
                                            "Yellow",
                                            "Green",
                                            "Blue"
                                        ]
                                    },
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        title: {
                                            display: true,
                                            text: 'Awesome Donut Chart Here',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        },
                                        animation: {
                                            animateScale: true,
                                            animateRotate: true
                                        }
                                    }
                                };

                            function chartsanimate( domEl ){

                                var options = {
                                  useEasing: true, 
                                  useGrouping: true, 
                                  separator: ',', 
                                  decimal: '.', 
                                };
                                var number1 = new CountUp('number1', 0, 2273270, 0, 3, options);
                                var number2 = new CountUp('number2', 0, 221096, 0, 3, options);
                                var number3 = new CountUp('number3', 0, 133265, 0, 3, options);
                                var number4 = new CountUp('number4', 0, 52, 0, 3, options);
                                if (!number1.error) {
                                  number1.start();
                                  number2.start();
                                  number3.start();
                                  number4.start();
                                } else {
                                  console.error(number1.error);
                                }
                                var ctx = document.getElementById("canvas-revenue").getContext("2d");
                                window.myBar = new Chart(ctx, {
                                    type: 'line',
                                    data: renenue_chart,
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        tooltips: {
                                            mode: 'index',
                                            intersect: false,
                                        },
                                        hover: {
                                            mode: 'nearest',
                                            intersect: true
                                        },
                                        title: {
                                            display: true,
                                            text: 'Revenue ($,million)',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        },
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    // Include a dollar sign in the ticks
                                                    callback: function(value, index, values) {
                                                        return '$' + value;
                                                    }
                                                }
                                            }]
                                        }
                                    }
                                });


                                var pc1 = document.getElementById("pie-chart").getContext("2d");
                                window.myPie = new Chart(pc1, pie1);

    //                             var cldc = document.getElementById("canvas-clients").getContext("2d");
    //                             window.clientsbar = new Chart(cldc, {
    //                                 type: 'bar',
    //                                 data: clientsdata,
    //                                 options: {
    //                                     responsive: true,
    //                                     legend: {
    //                                         position: 'bottom',
    //                                     },
    //                                     scaleLabel:
    // function(label){return  '$' + label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");},
    //                                     title: {
    //                                         display: true,
    //                                         text: 'Culturally and Linguistically Diverse Clients',
    //                                         fontSize: 18,
    //                                         fontStyle: 'normal',
    //                                         fontFamily: "'Poppins', sans-serif"
    //                                     }
    //                                 }
    //                             });

                                // var gta = document.getElementById("canvas-growth").getContext("2d");
                                // window.growthbar = new Chart(gta, {
                                //     type: 'line',
                                //     data: growthchart,
                                //     options: {
                                //         responsive: true,
                                //         legend: {
                                //             position: 'bottom',
                                //         },
                                //         title: {
                                //             display: true,
                                //             text: 'Culturally and Linguistically Diverse Clients'
                                //         },
                                //         scales: {
                                //             xAxes: [{
                                //                 scaleLabel: {
                                //                     display: true,
                                //                     labelString: 'Month'
                                //                 }
                                //             }],
                                //             yAxes: [{
                                //                 stacked: true,
                                //                 scaleLabel: {
                                //                     display: true,
                                //                     labelString: 'Value'
                                //                 }
                                //             }]
                                //         }
                                //     }
                                // });

                            };




                            var locations = [
                                ['<h3>Ipswich</h3>        <p><strong>Staff:</strong><span>25 </span><br><strong>KM Travelled:</strong><span> 271,963</span><br><strong>No. of Vehicles:</strong><span> 9</span><br><strong>Transport Trips:</strong><span> 25,560</span><br><strong>Home Care Packages:</strong><span> 7</span><br><strong>No. of Registered Clients:</strong><span> 3,042</span><br><strong>No. of Service Hrs</strong><span> 33,838</p>', -27.695544, 152.685365, 1],
                                ['<h3>Brisbane South</h3> <p><strong>Staff:</strong><span>1  </span><br><strong>KM Travelled:</strong><span> 1,860</span> <br><strong>No. of Vehicles:</strong><span> -</span><br><strong>Transport Trips:</strong><span> 266</span><br><strong>Home Care Packages:</strong><span> -</span><br><strong>No. of Registered Clients:</strong><span> 21</span><br><strong>No. of Service Hrs</strong><span> -</p>', -27.476650, 153.016670, 2],
                                ['<h3>Brisbane North</h3> <p><strong>Staff:</strong><span>7  </span><br><strong>KM Travelled:</strong><span> 192,522</span><br><strong>No. of Vehicles:</strong><span> 4</span><br><strong>Transport Trips:</strong><span> 15,619</span><br><strong>Home Care Packages:</strong><span> -</span><br><strong>No. of Registered Clients:</strong><span> 1473</span><br><strong>No. of Service Hrs</strong><span> 5,487</p>', -27.461179, 153.021497, 3],
                                ['<h3>Cabool</h3>         <p><strong>Staff:</strong><span>11 </span><br><strong>KM Travelled:</strong><span> 278,681</span><br><strong>No. of Vehicles:</strong><span> 6</span><br><strong>Transport Trips:</strong><span> 23,428</span><br><strong>Home Care Packages:</strong><span> 2</span><br><strong>No. of Registered Clients:</strong><span> 2209</span><br><strong>No. of Service Hrs</strong><span> 12,805</p>', -27.899833, 151.945599, 4],
                                ['<h3>Sunshine Coast</h3> <p><strong>Staff:</strong><span>57 </span><br><strong>KM Travelled:</strong><span> 1,194,775</span><br><strong>No. of Vehicles:</strong><span> 23</span><br><strong>Transport Trips:</strong><span> 112,147</span><br><strong>Home Care Packages:</strong><span> 22</span><br><strong>No. of Registered Clients:</strong><span> 14,436</span><br><strong>No. of Service Hrs</strong><span> 46,681</p>', -26.650000, 153.066667, 5],
                                ['<h3>Bundaberg</h3>      <p><strong>Staff:</strong><span>24 </span><br><strong>KM Travelled:</strong><span> 183,489</span><br><strong>No. of Vehicles:</strong><span> 6</span><br><strong>Transport Trips:</strong><span> 22,663</span><br><strong>Home Care Packages:</strong><span> 18</span><br><strong>No. of Registered Clients:</strong><span> 2,365</span><br><strong>No. of Service Hrs</strong><span> 26,443</p>', -24.866974, 152.350971, 6],
                                ['<h3>Townsville</h3>     <p><strong>Staff:</strong><span>4  </span><br><strong>KM Travelled:</strong><span> 123,048</span><br><strong>No. of Vehicles:</strong><span> 4</span><br><strong>Transport Trips:</strong><span> 14,362</span><br><strong>Home Care Packages:</strong><span> 3</span><br><strong>No. of Registered Clients:</strong><span> 1,853</span><br><strong>No. of Service Hrs</strong><span> 8,001</p>', -19.258964, 146.816948, 7],
                                ['<h3>Cairns</h3>         <p><strong>Staff:</strong><span>4  </span><br><strong>KM Travelled:</strong><span> 27,499</span><br><strong>No. of Vehicles:</strong><span> 1</span><br><strong>Transport Trips:</strong><span> 7,051</span><br><strong>Home Care Packages:</strong><span> -</span><br><strong>No. of Registered Clients:</strong><span> 347</span><br><strong>No. of Service Hrs</strong><span> 9</p>', -16.918551, 145.778055, 8],
                              ];

                              var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 6,
                                center: new google.maps.LatLng(-21.917574, 147.702796),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                disableDefaultUI: true,
                                scaleControl: true,
                                zoomControl: true,
                                styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]

                              });

                              var infowindow = new google.maps.InfoWindow();

                              var marker, i;
                              var bicon = {
                                path: 'M182.9,551.7c0,0.1,0.2,0.3,0.2,0.3S358.3,283,358.3,194.6c0-130.1-88.8-186.7-175.4-186.9   C96.3,7.9,7.5,64.5,7.5,194.6c0,88.4,175.3,357.4,175.3,357.4S182.9,551.7,182.9,551.7z M122.2,187.2c0-33.6,27.2-60.8,60.8-60.8   c33.6,0,60.8,27.2,60.8,60.8S216.5,248,182.9,248C149.4,248,122.2,220.8,122.2,187.2z',
                                fillColor: 'white',
                                fillOpacity: 1,
                                scale: .08,
                                strokeColor: '#006d92',
                                strokeWeight: 2
                              }
                              for (i = 0; i < locations.length; i++) { 
                                marker = new google.maps.Marker({
                                  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                  icon: bicon,
                                  map: map
                                });

                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                  return function() {
                                    infowindow.setContent(locations[i][0]);
                                    infowindow.open(map, marker);
                                  }
                                })(marker, i));
                              }

                            



                              $('.launch-modal').on('click', function(e){
                                e.preventDefault();
                                $( '#' + $(this).data('modal-id') ).modal();

                                var theModal = $(this).data("target"),
                                videoSRC = $(this).attr("data-video"),
                                videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=1&showinfo=0&html5=1&autoplay=1";
                                $(theModal + ' iframe').attr('src', videoSRCauto);
                                $(theModal + ' button.close').click(function () {
                                  $(theModal + ' iframe').attr('src', videoSRC);
                                });
                            });



} (jQuery) );

