(function($) {
    
  'use strict';


  /**
   * =====================================
   * Function for windows height and width      
   * =====================================
   */
  function windowSize( el ) {
    var result = 0;
    if("height" == el)
        result = window.innerHeight ? window.innerHeight : $(window).height();
    if("width" == el)
      result = window.innerWidth ? window.innerWidth : $(window).width();

    return result; 
  }


  /**
   * =====================================
   * Function for email address validation         
   * =====================================
   */
  function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
  };


  /**
   * =====================================
   * Function for windows height and width      
   * =====================================
   */
  function deviceControll() {
    if( windowSize( 'width' ) < 768 ) {
      $('body').removeClass('desktop').removeClass('tablet').addClass('mobile');
    }
    else if( windowSize( 'width' ) < 992 ){
      $('body').removeClass('mobile').removeClass('desktop').addClass('tablet');
    }
    else {
      $('body').removeClass('mobile').removeClass('tablet').addClass('desktop');
    }
  }




  $(window).on('resize', function() {

    deviceControll();

  });



  $(document).on('ready', function() {

    deviceControll();



    /**
     * =======================================
     * Top Navigaion Init
     * =======================================
     */
    // var navigation = $('#js-navbar-menu').okayNav({
    //   toggle_icon_class: "okayNav__menu-toggle",
    //   toggle_icon_content: "<span /><span /><span /><span /><span />"
    // });



    /**
     * =======================================
     * Top Fixed Navbar
     * =======================================
     */
    $(document).on('scroll', function() {
      var activeClass = 'fixed',
          ActiveID        = '#navigation',
          scrollPos       = $(this).scrollTop();

      if( scrollPos > 700 ) {
        $( ActiveID ).addClass( activeClass );
      } else {
        $( ActiveID ).removeClass( activeClass );
      }
    });




    /**
     * =======================================
     * NAVIGATION SCROLL
     * =======================================
     */
    var TopOffsetId = '.navbar-brand';
    $('#navbar-nav').onePageNav({
        currentClass: 'active',
        scrollThreshold: 0.2, // Adjust if Navigation highlights too early or too late
        scrollSpeed: 1000,
        scrollOffset: 50
     //   scrollOffset: Math.abs( $( TopOffsetId ).outerHeight() - 1 )
    });

    $('.btn, a.btn-nav').on('click', function (e) {
      e.preventDefault();

      var target = this.hash,
          scrollOffset = 50,
          $target = ( $(target).offset() || { "top": NaN }).top;

      $('html, body').stop().animate({
        'scrollTop': $target - scrollOffset
      }, 900, 'swing', function () {
        window.location.hash = target;
      });

    });


    /**
     * =======================================
     * PopUp Item Script
     * =======================================
     */
    $('.popup-video').magnificPopup({
      //disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: true,
      fixedContentPos: true
    });


    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;
    function onYouTubeIframeAPIReady() {
      player = new YT.Player('myplayer', {
        events: {
          'onStateChange': onPlayerStateChange
        }
      });
    }

    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
        $(".div_popup_overlay").fadeIn();
      }
      else if (event.data == YT.PlayerState.PAUSED || event.data == YT.PlayerState.ENDED) {
        $(".div_popup_overlay").fadeOut();
      }
    }
  


     /**
     * =======================================
     * TESTIMONIAL SYNC WITH CLIENTS
     * =======================================
     */
    var testimonialSlider = $(".testimonial-wrapper"); // client's message
    testimonialSlider.owlCarousel({
      singleItem :        true,
      autoPlay :          3000,
      slideSpeed :        500,
      paginationSpeed :   500,
      autoHeight :        false,
      navigation:         false,
      pagination:         true,
      // transitionStyle:    "fade"
    });




    /**
     * ============================
     * CONTACT FORM 2
     * ============================
    */
    $("#contact-form").on('submit', function(e) {
      e.preventDefault();
      var success = $(this).find('.email-success'),
        failed = $(this).find('.email-failed'),
        loader = $(this).find('.email-loading'),
        postUrl = $(this).attr('action');

      var data = {
        name: $(this).find('.contact-name').val(),
        email: $(this).find('.contact-email').val(),
        subject: $(this).find('.contact-subject').val(),
        message: $(this).find('.contact-message').val()
      };

      if ( isValidEmail(data['email']) && (data['message'].length > 1) && (data['name'].length > 1) ) {
        $.ajax({
          type: "POST",
          url: postUrl,
          data: data,
          beforeSend: function() {
            loader.fadeIn(1000);
          },
          success: function(data) {
            loader.fadeOut(1000);
            success.delay(500).fadeIn(1000);
            failed.fadeOut(500);
          },
          error: function(xhr) { // if error occured
            loader.fadeOut(1000);
            failed.delay(500).fadeIn(1000);
            success.fadeOut(500);
          },
          complete: function() {
            loader.fadeOut(1000);
          }
        });
      } else {
        loader.fadeOut(1000);
        failed.delay(500).fadeIn(1000);
        success.fadeOut(500);
      }

      return false;
    });


      var config = {
        afterReveal: chartsanimate
      }


      // Scroll Reveal
      window.sr = ScrollReveal(config);
      sr.reveal('.row');


  });

  
//    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                            var color = Chart.helpers.color;
                            var barChartData = {
                                labels: ["11/12", "12/13", "13/14", "14/15", "15/16", "16/17",],
                                datasets: [{
                                    label: 'City',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        5, 20, 40, 1800, 1900, 2500
                                    ]
                                }, {
                                    label: 'Regional',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                         2100, 2200, 2500, 4200, 4800, 5200
                                    ]
                                },
                                {
                                    label: 'Rural',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                         200, 200, 200, 500, 600, 1200
                                    ]
                                }]

                            };


                             var clientsdata = {
                                labels: ["11/12", "12/13", "13/14", "14/15", "15/16", "16/17",],
                                datasets: [{
                                    label: 'Number of Clients',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        18, 45, 55, 145, 160, 260
                                    ]
                                }]

                            };


                             var growthchart = {
                                labels: ["11/12", "12/13", "13/14", "14/15", "15/16", "16/17",],
                                datasets: [{
                                    label: 'Health',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        2000, 2200, 2400, 6000, 7000, 8000
                                    ]
                                },{
                                    label: 'Shopping',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        2500, 3500, 4000, 8000, 10000, 16000
                                    ]
                                },{
                                    label: 'Social',
                                    backgroundColor: color(window.chartColors.blue).alpha(0.8).rgbString(),
                                    borderColor: window.chartColors.blue,
                                    borderWidth: 1,
                                    data: [
                                        3200, 6500, 8000, 17000, 19000, 26000
                                    ]
                                }
                                ]

                            };

                                var randomScalingFactor = function() {
                                    return Math.round(Math.random() * 100);
                                };

                                var pie1 = {
                                    type: 'pie',
                                    data: {
                                        datasets: [{
                                            data: [
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                            ],
                                            backgroundColor: [
                                                window.chartColors.red,
                                                window.chartColors.orange,
                                                window.chartColors.yellow,
                                                window.chartColors.green,
                                                window.chartColors.blue,
                                            ],
                                            label: 'Dataset 1'
                                        }],
                                        labels: [
                                            "Red",
                                            "Orange",
                                            "Yellow",
                                            "Green",
                                            "Blue"
                                        ]
                                    },
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        tooltips: {
                                            mode: 'nearest'
                                        },
                                        title: {
                                            display: true,
                                            text: 'My Cool Chart Here',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        },
                                        animation: {
                                            animateScale: true,
                                            animateRotate: true
                                        }
                                    }
                                };

                                var donut = {
                                    type: 'doughnut',
                                    data: {
                                        datasets: [{
                                            data: [
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                                randomScalingFactor(),
                                            ],
                                            backgroundColor: [
                                                window.chartColors.red,
                                                window.chartColors.orange,
                                                window.chartColors.yellow,
                                                window.chartColors.green,
                                                window.chartColors.blue,
                                            ],
                                            label: 'Dataset 1'
                                        }],
                                        labels: [
                                            "Red",
                                            "Orange",
                                            "Yellow",
                                            "Green",
                                            "Blue"
                                        ]
                                    },
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        title: {
                                            display: true,
                                            text: 'Awesome Donut Chart Here',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        },
                                        animation: {
                                            animateScale: true,
                                            animateRotate: true
                                        }
                                    }
                                };

                            function chartsanimate( domEl ){

                                var pc1 = document.getElementById("pie-chart").getContext("2d");
                                window.myPie = new Chart(pc1, pie1);

                                var pc2 = document.getElementById("donut-chart").getContext("2d");
                                window.myPie2 = new Chart(pc2, donut);

                                var ctx = document.getElementById("canvas-geographic-growth").getContext("2d");
                                window.myBar = new Chart(ctx, {
                                    type: 'bar',
                                    data: barChartData,
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        title: {
                                            display: true,
                                            text: 'Geographical Growth',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        }
                                    }
                                });


                                var cldc = document.getElementById("canvas-clients").getContext("2d");
                                window.clientsbar = new Chart(cldc, {
                                    type: 'bar',
                                    data: clientsdata,
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        title: {
                                            display: true,
                                            text: 'Culturally and Linguistically Diverse Clients',
                                            fontSize: 18,
                                            fontStyle: 'normal',
                                            fontFamily: "'Poppins', sans-serif"
                                        }
                                    }
                                });

                                var gta = document.getElementById("canvas-growth").getContext("2d");
                                window.growthbar = new Chart(gta, {
                                    type: 'line',
                                    data: growthchart,
                                    options: {
                                        responsive: true,
                                        legend: {
                                            position: 'bottom',
                                        },
                                        title: {
                                            display: true,
                                            text: 'Culturally and Linguistically Diverse Clients'
                                        },
                                        scales: {
                                            xAxes: [{
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: 'Month'
                                                }
                                            }],
                                            yAxes: [{
                                                stacked: true,
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: 'Value'
                                                }
                                            }]
                                        }
                                    }
                                });

                            };

                            







} (jQuery) );

